\sect{Aufgabe 3.3 - Teilaufgabe 1}

Konstruieren Sie lineare Kongruenzen modulo 20, die keine, genau eine sowie mehr als eine Lösung haben. Gibt es eine mit 20 Lösungen?

\p{Keine Lösung}
$ \kongruenz{20X}{1}{20} $ \\
Da $ 20X $ immer ein vielfaches von $ 20 $ ist, teilt $ 20 $ niemals $ 20X - 1 $.

\p{Genau eine Lösung}
Eine lösbare lineare Kongruenz modulo 20 hat genau eine eine Lösung, wenn $ \ggT(a,20) = 1 $. Eine Möglichkeit wäre also $ \kongruenz{3X}{3}{20} $. Die einzige Lösung ist $ x_0 = 1 $.

\p{Mehrere Lösungen}
Eine lösbare lineare Kongruenz modulo 20 hat $ d $ Lösungen, wenn $ \ggT(a,20) = d $. Um $ 20 $ Lösungen zu haben, muss $ \ggT(a,20) = 20 $ sein, was z.B. für $ a = 20 $ der Fall ist. Die lineare Kongruenz $ \kongruenz{20X}{0}{20} $ hat als Lösungsmenge ganz $ \Z_{20} $, also 20 Lösungen.

\sect{Aufgabe 3.3 - Teilaufgabe 2}

Finden Sie alle ganzen Zahlen, die die Kongruenzen
\begin{center}
	$ \kongruenz{2X}{1}{5} $ \\
	$ \kongruenz{3X}{2}{7} $ \\
	$ \kongruenz{4X}{1}{11} $
\end{center}
gleichzeitig lösen.

\p{Lösung}

Da ein System linearer Kongruenzen nur dann mit Hilfe des chinesischen Restsatzes gelöst werden können, wenn sie die Form $ \kongruenz{X}{a_i}{m_i} $ haben, müssen diese Kongruenzen erst in ein Form gebracht werden, wo auf der linken Seite die selbe Zahl steht: \\

\begin{tabular}{l}
	$ \kongruenz{2X}{1}{5} $ \\
	$ \kongruenz{3X}{2}{7} $ \\
	$ \kongruenz{4X}{1}{11} $
\end{tabular}
$ \Rightarrow  $
\begin{tabular}{l}
	$ \kongruenz{12X}{6}{5} $ \\
	$ \kongruenz{12X}{8}{7} $ \\
	$ \kongruenz{12X}{3}{11} $
\end{tabular} \\

Mit $ X' = 12X $ gilt: \\
\begin{tabular}{l}
	$ \kongruenz{X'}{6}{5} $ \\
	$ \kongruenz{X'}{8}{7} $ \\
	$ \kongruenz{X'}{3}{11} $
\end{tabular} \\
\vdist \\


Seien $ a_1 = 6, a_2 = 8, a_3 = 3 $. \\
Seien $ m_1 = 5, m_2 = 7, m_3 = 11 $. \\
Sei $ M = m_1 m_2 m_3 = 5 \mal 7 \mal 11 = 385 $. \\
Seien $ M_1 = m_2 m_3 = 77, M_2 = m_1 m_3 = 55, M_3 = m_1 m_2 = 35 $. \\


\heading{Lösung $ x_1 $ der Kongruenz $\mathbf{ \kongruenz{M_1X_1}{1}{m_1} } $}

$ \kongruent{M_1X_1}{1}{m_1} \eq \kongruenz{77X_1}{1}{5} $. \\

Da $ \ggT(77,5) = 1 $ ist löst $ x_1 = s_1 \mod m_1 $ mit $ 1 = 77s_1 + 5t_1 $ diese Kongruenz. \\

Bestimmung von $ s_1 $ mit Hilfe des erweiterten Euklidischen Algorithmus: \\

\begin{tabular}{lllll}
	$ \ggT(77,5) $ & $ = \ggT(5,2) $ & $ (77 = 15 \mal 5 + 2 $ & $ \eq $ & $ 2 = 77 - 15 \mal 5) $ \\ 
	$ \ggT(5,2) $  & $ = \ggT(2,1) $ & $ (5 = 2 \mal 2 + 1 $   & $ \eq $ & $ 1 = 1 \mal 5 - 2 \mal 2) $ \\
	$ \ggT(2,1) $  & $ =\ggT(1,0) $  & $ (2 = 2 \mal 1 + 0) $  & &
\end{tabular} \\
\vdist \\
$
\Rightarrow 1 = 1 \mal 5 - 2 \mal (77 - 15 \mal 5) = 1 \mal 5 - 2 \mal 77 + 30 \mal 5 = -2 \mal 77 + 31 \mal 5 \\
\Rightarrow s_1 = -2 \\
\Rightarrow x_1 = -2 \mod 5 = 3
$ \\

\heading{Lösung $ x_2 $ der Kongruenz $ \mathbf{ \kongruenz{M_2X_2}{1}{m_2}} $}

$ \kongruenz{M_2X_2}{1}{m_2} \eq \kongruenz{55X_2}{1}{7} $. \\

Da $ \ggT(55,7) = 1 $ löst $ x_2 = s_2 \mod m_2 $ mit $ 1 = 55s_2 + 7t_2 $ diese Kongruenz. \\

Bestimmung von $ s_2 $ und mit Hilfe des erweiterten Euklidischen Algorithmus: \\

\begin{tabular}{lllll}
	$ \ggT(55,7) $ & $ = \ggT(7,6) $ & $ ( 55 = 7 \mal 7 + 6 $ & $ \eq $ & $ 6 = 55 - 7 \mal 7) $ \\
	$ \ggT(7,6)  $ & $ = \ggT(6,1) $ & $ ( 7 = 1 \mal 6 + 1 $  & $ \eq $ & $ 1 = 7 - 1 \mal 6 ) $ \\
	$ \ggT(6,1)  $ & $ = \ggT(1,0) $ & $ ( 6 = 6 \mal 1 + 0 ) $ & & \\
\end{tabular} \\
\vdist \\
$ 
\Rightarrow 1 = 7 - 1 \mal 6 = 7 - 1 \mal (55 - 7 \mal 7) = 7 - 55 + 7 \mal 7 = -1 \mal 55 + 8 \mal 7 \\
\Rightarrow s_2 = -1 \\
\Rightarrow x_2 = -1 \mod 7 = 6 $. \\


\heading{Lösung $ x_3 $ der Kongruenz $ \mathbf{ \kongruenz{M_3X_3}{1}{m_3} } $} \\
$ \kongruenz{M_3X_3}{1}{m_3} \eq \kongruenz{35X_3}{1}{11} $. \\
Da $ \ggT(35,11) = 1 \Rightarrow x_3 $ löst $ \kongruenz{35}{1}{11} $ mit $ x_3 = s_3 \mod m_3, 1 = 35s_3 + 11t_3 $. \\

Bestimme $ s_3 $ mit Hilfe des erweitertem Euklidischen Algorithmus: \\

\begin{tabular}{lllll}
	$ \ggT(35,11) $ & $ = \ggT(11,2) $ & $ ( 35 = 3 \mal 11 + 2 $ & $ \eq $ & $ 2 = 35 - 3 \mal  11) $ \\
	$ \ggT(11,2) $  & $ = \ggT(2,1) $ & $ ( 11 = 5 \mal 2 + 1 $   & $ \eq $ & $ 1 = 11 - 5 \mal  2) $ \\
	$ \ggT(2,1) $  & $ = \ggT(1,0) $ & $ ( 2 = 2 \mal 1 + 0 ) $   & &  \\
\end{tabular} \\
\vdist \\
$ 
\Rightarrow 1 = 11 - 5 \mal 2 = 11 - 5 \mal ( 35 - 3 \mal 11 ) = 11 - 5 \mal 35 + 15 \mal 11 = -5 \mal 35 + 16 \mal 11 \\
\Rightarrow s_3 = -5 \\
\Rightarrow x_3 = -5 \mod 11 = 6
$ \\

\vdist
Berechnung der Lösungen für das Kongruenzen System: \\

Sei $ x_0 = (a_1 M_1 x_1 + a_2 M_2 x_2 + a_3 M_3 x_3) \mod M = 6 \mal 77 \mal 3 + 8 \mal 55 \mal 6 + 3 \mal 35 \mal 6) \mod 385 = 4656 \mod 385 = 36 $, dann ist $ x_0 $ die einzige Lösung des Systems in $ \Z_M $. \\

Die Lösungsmenge des Systems \\

\begin{tabular}{l}
	$ \kongruenz{X'}{6}{5} $ \\
	$ \kongruenz{X'}{8}{7} $ \\
	$ \kongruenz{X'}{3}{11} $
\end{tabular} \\

ist also $ \menge[n \in \Z]{36 + 385 n} $. \\

$ \Rightarrow 12X = 36 + 385 n \eq X = 3 + \frac{385 n}{12} $. \\

Es kommen also nur die Werte für $ n $ in frage, für die $ 12 \teilt (385 n) $ gilt. \\

Da $ 385 n = 5 \mal 7 \mal 11 \mal n $ und jeder der bekannten Faktoren mit $ 12 $ teilerfremd ist, gilt $ 12 | 385n $ nur für $ n = 12k, k \in \Z $. \\

$ \Rightarrow X = 3 + \frac{385 \mal 12 \mal k}{12} = 3 + (385 k) $ \\

$ \Rightarrow 3 + (385 k) $ löst alle Kongruenzen des Systems \\
\begin{tabular}{l}
	$ \kongruenz{12X}{6}{5} $ \\
	$ \kongruenz{12X}{8}{7} $ \\
	$ \kongruenz{12X}{3}{11} $
\end{tabular}.\\

Da $ \ggT(5,6) = \ggT(7,4) = \ggT(11,3) = 1 $ gilt: \\

\begin{tabular}{lll}
	$ \kongruenz{12X}{6}{5}  $ & $ \Rightarrow $ & $ \kongruenz{2X}{1}{5} $ \\
	$ \kongruenz{12X}{8}{7}  $ & $ \Rightarrow $ & $ \kongruenz{3X}{2}{7}$ \\
	$ \kongruenz{12X}{3}{11} $ & $ \Rightarrow $ & $ \kongruenz{4X}{1}{11}$.
\end{tabular}\\

Also sind ist $ \menge[X = 3 + (385 k), k \in \Z]{X} $ die gesuchte Lösungsmenge.